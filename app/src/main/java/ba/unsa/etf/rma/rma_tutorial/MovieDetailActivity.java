package ba.unsa.etf.rma.rma_tutorial;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MovieDetailActivity extends AppCompatActivity {
    public TextView movie_title;
    public TextView movie_genre;
    public TextView movie_overview;
    public TextView movie_link;
    public TextView movie_date;
    public ListView actor_list;
    public ImageView movie_icon;
    public Button share_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        movie_title = findViewById(R.id.movie_title);
        movie_date = findViewById(R.id.movie_date);
        movie_genre = findViewById(R.id.movie_genre);
        movie_overview = findViewById(R.id.movie_overview);
        movie_link = findViewById(R.id.movie_link);

        movie_title.setText(getIntent().getStringExtra("title"));
        movie_overview.setText(getIntent().getStringExtra("overview"));
        movie_link.setText(getIntent().getStringExtra("homepage"));
        movie_genre.setText(getIntent().getStringExtra("genre"));
        movie_date.setText(getIntent().getStringExtra("date"));

        movie_icon = findViewById(R.id.movie_icon);
        if(getIntent().getStringExtra("genre").equals("Action"))
            movie_icon.setImageResource(R.mipmap.ic_action_round);
        else if(getIntent().getStringExtra("genre").equals("Drama"))
            movie_icon.setImageResource(R.mipmap.ic_drama_round);
        else if(getIntent().getStringExtra("genre").equals("Sci-Fi"))
            movie_icon.setImageResource(R.mipmap.ic_scifi_round);
        else movie_icon.setImageResource(R.mipmap.ic_movie_round);

        movie_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = getIntent().getStringExtra("homepage");
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });

        movie_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://www.youtube.com/results?search_query=" + getIntent().getStringExtra("title") + " trailer";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                MovieDetailActivity.this.startActivity(intent);
            }
        });

        share_button = findViewById(R.id.share_button);
        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, getIntent().getStringExtra("overview"));
                intent.setType("text/plain");
                MovieDetailActivity.this.startActivity(intent);
            }
        });
    }
}
