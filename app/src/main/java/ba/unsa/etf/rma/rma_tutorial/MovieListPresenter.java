package ba.unsa.etf.rma.rma_tutorial;

public class MovieListPresenter {
    private MovieListInteractor movieListInteractor;

    public MovieListInteractor getMovieListInteractor() {
        return movieListInteractor;
    }

    public void setMovieListInteractor(MovieListInteractor movieListInteractor) {
        this.movieListInteractor = movieListInteractor;
    }

    public MovieListPresenter() {
        movieListInteractor = new MovieListInteractor();
    }
}
