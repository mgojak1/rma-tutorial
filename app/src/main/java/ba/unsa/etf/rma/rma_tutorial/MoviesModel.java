package ba.unsa.etf.rma.rma_tutorial;

import java.util.ArrayList;

final class MoviesModel {
    private static ArrayList<Movie> movies = new ArrayList<>();
    static {
        movies.add(new Movie("Interstellar", "Sci-Fi", "07.11.2014", "https://www.imdb.com/title/tt0816692/",
                "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival"));
        movies.add(new Movie("Pulp Fiction", "Action", "14.10.1994", "https://www.imdb.com/title/tt0110912/",
                "The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption."));
        movies.add(new Movie("The Godfather", "Crime", "24.03.1972", "https://www.imdb.com/title/tt0068646/",
                "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son."));
        movies.add(new Movie("The Intouchables", "Drama", "02.11.2011", "https://www.imdb.com/title/tt1675434/",
                "After he becomes a quadriplegic from a paragliding accident, an aristocrat hires a young man from the projects to be his caregiver."));
        movies.add(new Movie("The Lord of the Rings", "Fantasy", "02.11.1998", "https://www.imdb.com/title/tt0120737/",
                "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron."));
    }

    static ArrayList<Movie> getMovies() {
        return movies;
    }
}
