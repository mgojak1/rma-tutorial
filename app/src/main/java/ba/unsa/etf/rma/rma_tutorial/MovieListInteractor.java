package ba.unsa.etf.rma.rma_tutorial;

public class MovieListInteractor {
    private MoviesModel moviesModel;

    public MovieListInteractor() {
    }

    public MoviesModel getMoviesModel() {
        return moviesModel;
    }

    public void setMoviesModel(MoviesModel moviesModel) {
        this.moviesModel = moviesModel;
    }
}
