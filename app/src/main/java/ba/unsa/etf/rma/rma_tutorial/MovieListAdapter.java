package ba.unsa.etf.rma.rma_tutorial;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MovieListAdapter extends ArrayAdapter<Movie> {
    private int resource;
    public TextView itemName;
    public ImageView icon;
    private Movie movie;
    private ArrayList<Movie> movies;

    public MovieListAdapter(Context context, int resource, List<Movie> objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.movies = new ArrayList<>(objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LinearLayout linearLayout;
        if(convertView == null) {
            linearLayout = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(inflater);
            layoutInflater.inflate(resource, linearLayout, true);
        }else {
            linearLayout = (LinearLayout)convertView;
        }
        movie = getItem(position);
        itemName = linearLayout.findViewById(R.id.itemName);
        itemName = linearLayout.findViewById(R.id.itemName);
        icon = linearLayout.findViewById(R.id.icon);
        itemName.setText(movie.getTitle());
        if(movie.getGenre().equals("Action"))
            icon.setImageResource(R.mipmap.ic_action_round);
        else if(movie.getGenre().equals("Drama"))
            icon.setImageResource(R.mipmap.ic_drama_round);
        else if(movie.getGenre().equals("Sci-Fi"))
            icon.setImageResource(R.mipmap.ic_scifi_round);
        else icon.setImageResource(R.mipmap.ic_movie_round);
        return linearLayout;
    }

    public Movie getMovie(int position) {
        return movies.get(position);
    }
}
