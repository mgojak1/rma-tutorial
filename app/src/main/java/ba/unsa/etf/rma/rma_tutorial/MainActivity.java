package ba.unsa.etf.rma.rma_tutorial;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private Button button;
    private EditText editText;
    private MovieListAdapter movieListAdapter;
    private MovieListPresenter movieListPresenter;
    private MovieBroadcastReceiver receiver;
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent movieDetailIntent = new Intent(MainActivity.this, MovieDetailActivity.class);
            Movie movie = movieListAdapter.getMovie(position);
            movieDetailIntent.putExtra("title", movie.getTitle());
            movieDetailIntent.putExtra("genre", movie.getGenre());
            movieDetailIntent.putExtra("homepage", movie.getHomepage());
            movieDetailIntent.putExtra("overview", movie.getOverview());
            movieDetailIntent.putExtra("date", movie.getReleaseDate());
            MainActivity.this.startActivity(movieDetailIntent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);
        button = findViewById(R.id.button);
        button.setEnabled(false);
        editText = findViewById(R.id.editText);
        movieListPresenter = new MovieListPresenter();
        movieListAdapter = new MovieListAdapter(this, R.layout.list_item, MoviesModel.getMovies());
        listView.setAdapter(movieListAdapter);
        View.OnClickListener listItemListener;
        listView.setOnItemClickListener(listItemClickListener);

        Intent intent = getIntent();
        String data = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (intent.getAction().equals(Intent.ACTION_SEND)) {
            editText.setText(data);
        }
        receiver = new MovieBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_SERVICE);
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            System.out.println("NOT NULL");
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }

}
